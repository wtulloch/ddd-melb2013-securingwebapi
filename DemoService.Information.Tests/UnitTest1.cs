﻿using System;
using System.Net;
using System.Net.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DemoService.Information.Tests
{
	[TestClass]
	public class InformationFixture
	{
		private Uri infoUri = new Uri("http://localhost:51796/api/information");

		[TestMethod]
		public void Get_shouldReturnAString()
		{
			using (var client = new HttpClient())
			{
				var response = client.GetAsync(infoUri).Result;
				Assert.AreEqual(HttpStatusCode.OK,response.StatusCode);

				var result = response.Content.ReadAsStringAsync().Result;
			}
		
		}

		[TestMethod]
		public void Post()
		{
			using (var client = new HttpClient())
			{
				var content = new StringContent("test");
				var Url = infoUri.AbsoluteUri + "/Add";
				var response = client.PostAsync(infoUri,content).Result;
				
				Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

				var result = response.Content.ReadAsStringAsync().Result;
			}

		}
	}
}
