﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Script.Serialization;
using TimeGhost.Security.Tools;

namespace TestHarness
{
	class Program
	{
		private static Uri personnelEndpointHttps = new Uri("https://localhost:44304/api/personnel/");
		private static Uri tokenEndpoint = new Uri("https://localhost:44304/api/token");
		private static Uri claimsEndpoint = new Uri("https://localhost:44304/api/user/claims");

		static void Main(string[] args)
		{
			Console.WriteLine("Calling Personnel Service");
			TokenResponse tokenResponse = null;

			using (var client = new HttpClient())
			{
				tokenResponse = LoginUser(client);
				if (tokenResponse != null)
				{

					GetUsersClaims(client, tokenResponse);
					Console.WriteLine();
					GetPersonnelRecords(client, tokenResponse);

				}

				Console.ReadLine();
			}
		}

		private static TokenResponse LoginUser(HttpClient client)
		{
			Console.WriteLine("Attempting to authenticate the user");
			Console.WriteLine();

			TokenResponse tokenResponse = null;
			client.DefaultRequestHeaders.Authorization = BasicAuthenticationHeaderValue
				.CreateAuthenticationHeaderValue("john.cale", "Pa$$w0rd");
			var response = client.GetAsync(tokenEndpoint).Result;

			switch (response.StatusCode)
			{
				case HttpStatusCode.OK:
					{
						var jsonString = response.Content.ReadAsStringAsync().Result;
						tokenResponse = GetTokenResponse(jsonString);

						Console.WriteLine("User authenticated we have a token:");
						Console.WriteLine(tokenResponse.AccessToken);
						Console.WriteLine();
					}
					break;
				case HttpStatusCode.Unauthorized:
					Console.WriteLine("{0}: {1}", response.StatusCode, response.Content.ReadAsStringAsync().Result);
					break;
			}
			return tokenResponse;
		}

		private static void GetPersonnelRecords(HttpClient client, TokenResponse tokenResponse)
		{
			client.DefaultRequestHeaders.Authorization =
				new AuthenticationHeaderValue("Bearer", tokenResponse.AccessToken);

			var response = client.GetAsync(personnelEndpointHttps).Result;
			switch (response.StatusCode)
			{
				case HttpStatusCode.OK:
					{
						var jsonString = response.Content.ReadAsStringAsync().Result;
						DisplayEmployees(jsonString);
					}
					break;
				case HttpStatusCode.Unauthorized:
					Console.WriteLine("{0}: {1}", response.StatusCode, response.Content.ReadAsStringAsync().Result);
					break;
				default:
					Console.WriteLine("{0}: {1}", response.StatusCode, response.Content.ReadAsStringAsync().Result);
					break;
			}
		}

		private static void GetUsersClaims(HttpClient client, TokenResponse tokenResponse)
		{
			client.DefaultRequestHeaders.Authorization =
				new AuthenticationHeaderValue("Bearer", tokenResponse.AccessToken);

			var response = client.GetAsync(claimsEndpoint).Result;
			if (response.StatusCode == HttpStatusCode.OK)
			{
				var jsonString = response.Content.ReadAsStringAsync().Result;
				DisplayUserClaims(jsonString);

			}
			else
			{
				Console.WriteLine("Oops!: {0}", response.StatusCode);
			}
		}



		private static void DisplayEmployees(string results)
		{
			var serialiser = new JavaScriptSerializer();
			var employees = serialiser.Deserialize<List<Employee>>(results);
			foreach (var employee in employees)
			{
				Console.WriteLine(" ({0}) {1} {2}: {3}", employee.Id, employee.FirstName, employee.LastName, employee.Email);
			}

		}

		private static TokenResponse GetTokenResponse(string jsonString)
		{
			var serialiser = new JavaScriptSerializer();
			return serialiser.Deserialize<TokenResponse>(jsonString);
		}

		private static void DisplayUserClaims(string jsonString)
		{
			var serialiser = new JavaScriptSerializer();
			var claims = serialiser.Deserialize<List<ViewClaim>>(jsonString);

			claims.ForEach(vc => Console.WriteLine("{0}: {1}", vc.Type, vc.Value));
		}
	}

	internal class TokenResponse
	{
		public string AccessToken { get; set; }
		public string TokenType { get; set; }
		public int ExpiresIn { get; set; }
	}

	internal class Employee
	{
		public int Id { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Username { get; set; }
		public string Email { get; set; }
	}

	internal class ViewClaim
	{
		public string Type { get; set; }
		public string Value { get; set; }
	}
}
