﻿namespace TimeGhost.Security.Tools
{
	public interface IAuthenticationRepository
	{
		bool IsAuthenticatedUser(string username, string password);

		User GetUserByUsername(string username);
	}
}