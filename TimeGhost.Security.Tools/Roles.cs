﻿namespace TimeGhost.Security.Tools
{
	public static class Roles
	{
		public const string Developer = "Developer";
		public const string User = "User";
		public const string Manager = "Manager";
	}
}