﻿using System;
using System.Net.Http.Headers;
using System.Text;

namespace TimeGhost.Security.Tools
{
	public class BasicAuthenticationHeaderValue
	{
		private static readonly Encoding Encoding = Encoding.GetEncoding("iso-8859-1");
		private const string Scheme = "Basic";


		/// <summary>
		/// This method returns an instance of AuthenicationHeaderValue with the username and password
		/// base-64 encoded
		/// </summary>
		/// <param name="userName"></param>
		/// <param name="password"></param>
		/// <returns></returns>
		/// <code>
		///	using (var client = new HttpClient())
		///		{
		//			client.DefaultRequestHeaders.Authorization = BasicAuthenticationHeaderValue.CreateAuthenticationHeaderValue(testUser.Username,testUser.Password);
		///      }
		/// </code>
		public static AuthenticationHeaderValue CreateAuthenticationHeaderValue(string userName, string password)
		{
			string credential = String.Format("{0}:{1}", userName, password);
			var encoded = Convert.ToBase64String(Encoding.GetBytes(credential));

			return new AuthenticationHeaderValue(Scheme, encoded);
		}

		//TODO:currently returning dictionary for convenience should probably return something else :-)
		public static UserCredentials GetUsernameAndPasswordFromHeaderValue(string encodedHeaderValue)
		{
			if (string.IsNullOrEmpty(encodedHeaderValue))
			{
				throw new ArgumentException("encodedHeaderValue cannot be null or empty");
			}

			var decoded = Convert.FromBase64String(encodedHeaderValue);
			var result = Encoding.GetString(decoded, 0, decoded.Length);
			var split = result.Split(':');

			var userCredentials = new UserCredentials
			{
				Username = split[0],
				Password = split[1]
			};

			return userCredentials;
		} 
	}
}