﻿using System.Collections.Generic;

namespace TimeGhost.Security.Tools
{
	public class User
	{
		public User()
		{
			Roles  = new List<string>();
		}
		public string Username { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Email { get; set; }
		public List<string> Roles { get; set; }
	}
}