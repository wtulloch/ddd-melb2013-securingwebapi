﻿namespace TimeGhost.Security.Tools
{
	public class CustomClaimTypes
	{
		public const string InformationResource = "urn:timeghost.information";
		public const string Scope = "urn:timeghost.Scope";
	}

	public class ClaimValues
	{
		public const string Read = "Read";
		public const string Write = "Write";
		public const string Update = "Update";
		public const string Delete = "Delete";
	}
}