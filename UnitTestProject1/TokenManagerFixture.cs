﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IdentityModel.Tokens;
using System.Security.Claims;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TimeGhost.Security.Tools;
using NSubstitute;

namespace TimeGhost.Security.Core.Tests
{
	[TestClass]
	public class TokenManagerFixture
	{
		[TestMethod]
		public void CreateJwtToken_GivenAValidUser_ReturnsAValidSecurityToken()
		{
			var validUser = new User
				{
					Username = "test.user",
					FirstName = "Test",
					LastName = "User",
					Email = "test.user@example.com",
					Roles = new List<string> {"developer"}
				};

			var claims = new List<Claim>
				{
					new Claim(ClaimTypes.Name, "test.user"),
					new Claim(ClaimTypes.NameIdentifier, "test.user")
				};
			var claimsIdentity = new ClaimsIdentity(claims, "test");

			var tokenConfig = new TokenConfiguration
				{
					AppliesTo = "http://test",
					Issuer = "testIssuer",
					ExpiresIn = 5
				};

			var fakeRepository = Substitute.For<IAuthenticationRepository>();
			fakeRepository.GetUserByUsername(validUser.Username).Returns(validUser);
			
			var tokenManager = new TokenManager(fakeRepository);

			var tokenResponse = tokenManager.CreateJwtToken(claimsIdentity, tokenConfig);

			var tokenHandler = new JwtSecurityTokenHandler();
			var tokenValidationParams = new TokenValidationParameters
				{
					AllowedAudience =  tokenConfig.AppliesTo,
					ValidIssuer = tokenConfig.Issuer,
					SigningToken = ManageTokenCredentials.GetBinarySecretSecurityToken(),
					ValidateIssuer = true
				};

				var claimsPrincipal =  tokenHandler.ValidateToken(tokenResponse.AccessToken,tokenValidationParams);
				
				Assert.AreEqual("test.user", claimsPrincipal.Identity.Name);
		

		}
	}
}
