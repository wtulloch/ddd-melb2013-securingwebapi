using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using WebServiceTests.Models.Mapping;

namespace WebServiceTests.Models
{
    public partial class TimeGhostDemoServiceContext : DbContext
    {
        static TimeGhostDemoServiceContext()
        {
            Database.SetInitializer<TimeGhostDemoServiceContext>(null);
        }

        public TimeGhostDemoServiceContext()
            : base("Name=TimeGhostDemoServiceContext")
        {
        }

        public DbSet<EmployeeRole> EmployeeRoles { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new EmployeeRoleMap());
            modelBuilder.Configurations.Add(new EmployeeMap());
            modelBuilder.Configurations.Add(new RoleMap());
        }
    }
}
