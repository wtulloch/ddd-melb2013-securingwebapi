using System;
using System.Collections.Generic;

namespace WebServiceTests.Models
{
    public partial class Employee
    {
        public Employee()
        {
            this.EmployeeRoles = new List<EmployeeRole>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public virtual ICollection<EmployeeRole> EmployeeRoles { get; set; }
    }
}
