using System;
using System.Collections.Generic;

namespace WebServiceTests.Models
{
    public partial class Role
    {
        public Role()
        {
            this.EmployeeRoles = new List<EmployeeRole>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<EmployeeRole> EmployeeRoles { get; set; }
    }
}
