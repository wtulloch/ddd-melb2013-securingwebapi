﻿using System;
using DemoService.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WebServiceTests
{
	[TestClass]
	public class EmployeeRepositoryTests
	{
		[TestMethod]
		public void GetAll_ShouldReturnAllEmployees()
		{
			var repository = new EmployeeRepository();

			var employees = repository.GetAll();

			Assert.IsTrue(employees.Count > 0);
		}

		[TestMethod]
		public void Delete_ShouldReduceEmployeesCountByOne()
		{
			var repository = new EmployeeRepository();

			var currentEmployeeCount = repository.GetAll().Count;

			repository.Delete(1);

			var newEmployeeCount = repository.GetAll().Count;

			Assert.AreEqual(currentEmployeeCount -1, newEmployeeCount);

		}
	}
}
