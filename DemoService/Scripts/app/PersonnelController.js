﻿function PersonnelController($scope, $http) {
    $scope.loading = true;

    $http.get('/api/personnel/').success(function(data) {
        $scope.employees = data;
        $scope.loading = false;
    })
        .error(function () {
            $scope.error = "An error has occured loading the employees";
            $scope.loading = false;
        });
}