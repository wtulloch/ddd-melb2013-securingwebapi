﻿ function demoAppController($scope, $http, $location) {
     var tokenRoute = "/api/token";
     var userRoute = "/api/User";
     var infoRoute = "/api/information";
     var tokenKey = "timeGhost-key";

     $scope.fullName = "";
     
     var isLoggedIn = false;
     
     var getLoginConfig = function (username, password) {
         var valueToEncode = username + ":" + password;
         var encodedValue = encode64(valueToEncode);
         var config = {
             headers: {
                 Authorization: "Basic " + encodedValue,
                 Accept: 'application/json'
             },
             withCredentials: true
         };
         return config;
     };
     var getRequestConfig = function () {
         var config = {
             headers: {
                 Accept: 'application/json',
                 Authorization: ''
             }             
         };
         var accessToken = amplify.store(tokenKey);
         if (accessToken) {
             config.headers.Authorization =  "Bearer " + accessToken;
             config.withCredentials = true;
         }
         return config;
     };

     var getUserDetails = function () {
         var config = getRequestConfig();
         $http.get(userRoute + "/UserDetails", config)
             .success(function (data, status, headers, config) {
                 $scope.fullName = data.FullName;
             })
             .error(function (data, status, headers, config) {
                 console.log("user details error", status + ": " + data);
                 isLoggedIn = false;
                 alert("access denied, please log in.");
             });
     };

     if (amplify.store(tokenKey)) {
         isLoggedIn = true;
         getUserDetails();
     }

    
     
     var loginSuccess = function (data, status, headers, config) {
         var accessToken = data.AccessToken;
         amplify.store(tokenKey, accessToken);
         $scope.password = "";
         $scope.information = "";
         isLoggedIn = true;
         getUserDetails();
     };

     var loginError = function(data, status, headers, config) {
         console.log(status);
         alert("Invalid username or password.");
     };

     var infoSuccess = function(data, status, headers, config) {
         $scope.information = data;
     };
     var infoError = function(data, status, headers, config) {
         if (status === 401) {
             $scope.information = "Oops me thinks you are not authorised for this";
         } else {
             $scope.information = "There was a problem: " + status;
         }
         console.log(status + ": " + data);
     };

     $scope.getInfo = function() {
         var config = getRequestConfig();
         $http.get(infoRoute, config)
             .success(infoSuccess)
             .error(infoError);
     };

     $scope.addInfo = function() {
         var config = getRequestConfig();
        
         $http.post(infoRoute, "new information" , config)
             .success(infoSuccess)
             .error(infoError);
     };
     
     $scope.updateInfo = function () {
         var config = getRequestConfig();
         $http.put(infoRoute, "updated information", config)
             .success(infoSuccess)
             .error(infoError);
     };

     $scope.deleteInfo = function() {
         var config = getRequestConfig();
         $http.delete(infoRoute + "/?id=2", config)
             .success(infoSuccess)
             .error(infoError);
     };
     
     $scope.viewSecrets = function () {
         var config = getRequestConfig();
         $http.get(infoRoute + "/?id=1", config)
             .success(infoSuccess)
             .error(infoError);
     };

     $scope.title = "TimeGhost Secrets";
     $scope.username = "";
     $scope.password = "";

     $scope.login = function() {
         var config = getLoginConfig($scope.username, $scope.password);
         $http.get(tokenRoute, config)
             .success(loginSuccess)
             .error(loginError);
     };

     $scope.logout = function() {
         isLoggedIn = false;
         $scope.information = "";
         amplify.store(tokenKey, null);
     };

    


     $scope.getUserIsLoggedIn = function () { return isLoggedIn; };
 }
 
 //function copied from http://ntt.cc/2008/01/19/base64-encoder-decoder-with-javascript.html
 function encode64(input) {
     var keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=@$.";

     var output = "";
     var chr1, chr2, chr3 = "";
     var enc1, enc2, enc3, enc4 = "";
     var i = 0;

     do {
         chr1 = input.charCodeAt(i++);
         chr2 = input.charCodeAt(i++);
         chr3 = input.charCodeAt(i++);

         enc1 = chr1 >> 2;
         enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
         enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
         enc4 = chr3 & 63;

         if (isNaN(chr2)) {
             enc3 = enc4 = 64;
         } else if (isNaN(chr3)) {
             enc4 = 64;
         }

         output = output +
            keyStr.charAt(enc1) +
            keyStr.charAt(enc2) +
            keyStr.charAt(enc3) +
            keyStr.charAt(enc4);
         chr1 = chr2 = chr3 = "";
         enc1 = enc2 = enc3 = enc4 = "";
     } while (i < input.length);

     return output;

 }

