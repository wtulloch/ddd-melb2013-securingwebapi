﻿using System;
using System.Net.Http;
using System.Web.Http.Dependencies;

namespace DemoService.Helpers
{
	public static class ExtensionMethods
	{
		public static bool IsHttpsRequest(this HttpRequestMessage message)
		{
			return string.Equals(Uri.UriSchemeHttps, message.RequestUri.Scheme, StringComparison.OrdinalIgnoreCase);
		}

		public static T GetService<T>(this IDependencyScope resolver)
		{
			return (T)resolver.GetService(typeof(T));
		}
	}
}