﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using DemoService.Models;

namespace DemoService.Controllers
{
    public class UserController : ApiController
    {
		[HttpGet]
		public List<ViewClaim> Claims()
		{
			var claims = new List<ViewClaim>();
			var principal = HttpContext.Current.User as ClaimsPrincipal;

			principal.Claims.ToList().ForEach(c => claims.Add(new ViewClaim{Type=c.Type, Value = c.Value}));

			return claims;
		}

		[HttpGet]
		public object UserDetails()
		{
			var principal = HttpContext.Current.User as ClaimsPrincipal;
			var firstName = principal.FindFirst(c => c.Type == ClaimTypes.GivenName).Value;
			var lastName = principal.FindFirst(c => c.Type == ClaimTypes.Surname).Value;
			var fullname = string.Format("{0} {1}", firstName, lastName);

			return new {FullName = fullname};
			;
		}
    }
}
