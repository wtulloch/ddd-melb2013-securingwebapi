﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DemoService.Models;

namespace DemoService.Controllers
{
    public class PersonnelController : ApiController
    {
	    private IRepository<EmployeeDto> _repository;

	    public PersonnelController(IRepository<EmployeeDto> repository )
	    {
		    _repository = repository;
	    }

		public List<EmployeeDto> Get()
		{
			List<EmployeeDto> employees = _repository.GetAll();
			return employees;
		}

		public EmployeeDto Get(int id)
		{
			var employeeDto =_repository.Get(id);
			return employeeDto;
		}

	    public EmployeeDto Post(EmployeeDto employee)
	    {
		    var newEployee = _repository.Add(employee);
		    return newEployee;
	    }

	    public void Put(EmployeeDto employee)
	    {
		    _repository.Update(employee);
	    }

		public void Delete(int id)
		{
			_repository.Delete(id);
		}
    }
}
