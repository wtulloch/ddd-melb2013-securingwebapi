﻿using System;
using System.Net;
using System.Net.Http;
using System.Security;
using System.Security.Claims;
using System.Threading;
using System.Web.Http;
using TimeGhost.Security.Core;

namespace DemoService.Controllers
{
	//Note for demo purposes hard coding the Token configuration.
    public class TokenController : ApiController
    {
	    private readonly ITokenManager _tokenManager;
	    private readonly TokenConfiguration _tokenConfiguration;

	    public TokenController(ITokenManager tokenManager)
	    {
		    _tokenManager = tokenManager;
		    _tokenConfiguration = new TokenConfiguration
			    {
					Issuer = "TimeGhost",
					AppliesTo = "www.timeghost.com.au",
					ExpiresIn = 10,
					SigningCredentials = ManageTokenCredentials.GetSymmetricKeySigningCredentials(),
					SigningSecurityToken = ManageTokenCredentials.GetBinarySecretSecurityToken()
			    };

	    }

		public HttpResponseMessage Get()
		{
			try
			{
				var claimsIdentity = ClaimsPrincipal.Current.Identity as ClaimsIdentity;
				var tokenResponse = _tokenManager.CreateJwtToken(claimsIdentity, _tokenConfiguration);
				var response = Request.CreateResponse(HttpStatusCode.OK, tokenResponse);
				return response;
			}
			catch (SecurityException)
			{
				return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Unauthorised user");

			}
			catch (Exception)
			{
				return Request.CreateErrorResponse(HttpStatusCode.BadRequest,"Oops");
			}
		}
    }
}
