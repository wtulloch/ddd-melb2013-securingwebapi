﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DemoService.Filters;

namespace DemoService.Controllers
{
    public class InformationController : ApiController
    {
		//[ClaimsAuthorise]
		public string Get()
		{
			return "Get: Here is some stuff you can look at.";
		}

		//[ClaimsAuthorise("Information", "Secrets")]
		[Authorize]
		public string Get(int id)
		{
			return "Get Secrets: Oh Crap!";
		}

		//[Authorize(Roles = "Developer, Manager")]
		//[ClaimsAuthorise]
		public string Post([FromBody] string newinfo)
		{
			return "Post: Gee, you must be pretty important you can add new information.";
		}

		//[ClaimsAuthorise]
		public string Put([FromBody] string updatedInfo)
		{
			return "Put: I hope you know what you are doing, changing data shouldn't be done lightly you know.";
		}

		//[ClaimsAuthorise]
		//[Authorize(Users="lou.reed")]
		public string Delete(int id)
		{
			return "Delete: I think I just clicked the wrong button.";
		}

	
    }

	
}
