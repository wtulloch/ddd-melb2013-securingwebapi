﻿using System;
using System.Net;
using System.Net.Http;

namespace DemoService.Handlers
{
	public class EnsureHttpsHandler : DelegatingHandler
	{
		protected override async System.Threading.Tasks.Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
		                                                                                    System.Threading.CancellationToken
			                                                                                    cancellationToken)
		{
			if (!string.Equals(request.RequestUri.Scheme, "https", StringComparison.OrdinalIgnoreCase))
			{
				return new HttpResponseMessage(HttpStatusCode.BadRequest)
					{
						Content = new StringContent("HTTPS required")
					};
			}
			var response = await base.SendAsync(request, cancellationToken);
			return response;
		}
	}
}