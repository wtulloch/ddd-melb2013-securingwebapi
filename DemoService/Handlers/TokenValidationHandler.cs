﻿using System;
using System.IdentityModel.Services;
using System.IdentityModel.Tokens;
using System.Net;

using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using TimeGhost.Security.Core;

namespace DemoService.Handlers
{
	public class TokenValidationHandler : DelegatingHandler
	{
		private ITokenManager _tokenManager;
		
		public TokenValidationHandler(ITokenManager tokenManager)
		{
			_tokenManager = tokenManager;
		}

		public TokenValidationHandler(HttpConfiguration configuration, ITokenManager tokenManager)
		{
			InnerHandler = new HttpControllerDispatcher(configuration);
			_tokenManager = tokenManager;
		}

		protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, System.Threading.CancellationToken cancellationToken)
		{
			string token;
			if (request.Headers.Authorization!=null && request.Headers.Authorization.Scheme.Equals("Basic", StringComparison.OrdinalIgnoreCase))
			{
				return base.SendAsync(request, cancellationToken);
			}

			if (!SecurityTokenHelper.TryRetrieveTokenValue(request, out token))
			{
				
				return Task.FromResult(request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Unauthorised"));
			}

			try
			{
				SetCurrentPrincipal(token);
			}
			catch (SecurityTokenException)
			{

				return Task.FromResult(request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Unauthorised"));
			}

			return  base.SendAsync(request, cancellationToken);
		}

		private void SetCurrentPrincipal(string token)
		{
			var tokenConfig = new TokenConfiguration
			{
				Issuer = "TimeGhost",
				AppliesTo = "www.timeghost.com.au",
				ExpiresIn = 10,
				SigningCredentials = ManageTokenCredentials.GetSymmetricKeySigningCredentials(),
				SigningSecurityToken = ManageTokenCredentials.GetBinarySecretSecurityToken()
			};

			var principal = _tokenManager.GetPrincipalFromJwtToken(token, tokenConfig);
			var transformer = FederatedAuthentication.FederationConfiguration.IdentityConfiguration.ClaimsAuthenticationManager;
			var transformedPrincipal = transformer != null ? transformer.Authenticate(string.Empty, principal): principal;

			Thread.CurrentPrincipal = transformedPrincipal;
			if (HttpContext.Current != null)
			{
				HttpContext.Current.User = transformedPrincipal;
			}

		}
	}
}