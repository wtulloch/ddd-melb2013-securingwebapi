﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;

namespace DemoService.Handlers
{
	internal class SecurityTokenHelper
	{
		internal static bool TryRetrieveTokenValue(HttpRequestMessage request, out string token)
		{
			token = null;
			var authHeader = request.Headers.Authorization;
			AuthenticationHeaderValue headerValue;
			if (authHeader == null)
				return false;

			if (!AuthenticationHeaderValue.TryParse(authHeader.ToString(), out headerValue) || 
				!headerValue.Scheme.Equals("Bearer", StringComparison.OrdinalIgnoreCase))
			{
				return false;
			}

			if (string.IsNullOrEmpty(headerValue.Parameter))
			{
				return false;
			}
			token = headerValue.Parameter;

			return true;

		}
	}
}