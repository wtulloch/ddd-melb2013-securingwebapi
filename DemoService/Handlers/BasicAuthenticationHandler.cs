﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Dependencies;
using System.Web.Http.Dispatcher;
using DemoService.Models;
using DemoService.Helpers;
using TimeGhost.Security.Tools;

namespace DemoService.Handlers
{
	public class BasicAuthenticationHandler: DelegatingHandler
	{
		private IAuthenticationRepository _repository;

		public BasicAuthenticationHandler(IAuthenticationRepository repository)
		{
			_repository = repository;
			
		}
		public BasicAuthenticationHandler(HttpConfiguration configuration, IAuthenticationRepository authenticationRepository)
		{
			_repository = authenticationRepository;
			InnerHandler = new HttpControllerDispatcher(configuration);
		}

		protected async override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, System.Threading.CancellationToken cancellationToken)
		{
			var authorisationHeader = request.Headers.Authorization;

			if (authorisationHeader == null || !AthenticatedUser(authorisationHeader))
			{
				return CreateErrorResponse(request);
			}

			return  await base.SendAsync(request, cancellationToken);
		}

		private static HttpResponseMessage CreateErrorResponse(HttpRequestMessage request)
		{
			HttpResponseMessage response = request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Unauthorised User");
			//response.Headers.WwwAuthenticate.Add(new AuthenticationHeaderValue("Basic"));
			return response;
		}

		private bool AthenticatedUser(AuthenticationHeaderValue authorisationHeader)
		{
			if (!authorisationHeader.Scheme.Equals("Basic", StringComparison.OrdinalIgnoreCase))
			{
				return false;
			}
			var userCredentials =
				BasicAuthenticationHeaderValue.GetUsernameAndPasswordFromHeaderValue(authorisationHeader.Parameter);
			var isValidUser = _repository.IsAuthenticatedUser(userCredentials.Username, userCredentials.Password);
			if (isValidUser)
			{
				SetNewPrincipal(userCredentials.Username, authorisationHeader.Scheme);
			}
			return isValidUser;
		}

		private void SetNewPrincipal(string username, string authenticationType)
		{
			var claims = new List<Claim>
			    {
				    new Claim(ClaimTypes.NameIdentifier, username),
				    new Claim(ClaimTypes.Name, username)
			    };
			var identity = new ClaimsIdentity(claims, authenticationType);

			var principal = new ClaimsPrincipal(identity);
			System.Threading.Thread.CurrentPrincipal = principal;
			if(HttpContext.Current != null)
			HttpContext.Current.User = principal;
		}
	}
}