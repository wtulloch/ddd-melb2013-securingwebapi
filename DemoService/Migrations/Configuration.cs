using DemoService.Models;

namespace DemoService.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DemoService.Models.EmployeeContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DemoService.Models.EmployeeContext context)
        {

	        context.Employees.AddOrUpdate(e => e.Id,
	                                      new Employee
		                                      {
			                                      Id = 1,
			                                      FirstName = "Maureen",
			                                      LastName = "Tucker",
			                                      Email = "Maureen.Tucker@TimeGhost.com.au",
												  Username = "maureen.tucker",
												  Password = "Pa$$w0rd"
		                                      },
	                                      new Employee
		                                      {
			                                      Id = 2,
			                                      FirstName = "Lou",
			                                      LastName = "Reed",
			                                      Email = "Lou.Reed@TimeGhost.com.au",
												  Username = "lou.reed",
												  Password = "Pa$$w0rd"
		                                      },
	                                      new Employee
		                                      {
			                                      Id = 3,
			                                      FirstName = "John",
			                                      LastName = "Cale",
			                                      Email = "John.Cale@TimeGhost.com.au",
												  Username = "john.cale",
												  Password = "Pa$$w0rd"
		                                      },
 new Employee
		                                      {
			                                      Id = 4,
			                                      FirstName = "Edward",
			                                      LastName = "Snowden",
			                                      Email = "edward.snowden@TimeGhost.com.au",
												  Username = "edward.snowden",
												  Password = "Pa$$w0rd"
		                                      }

		        );

			context.Roles.AddOrUpdate(r => r.Id,
					new Role{Id=1, Name="Developer"},
					new Role{Id =2, Name="Manager"},
					new Role{Id=3, Name="User"}
				);

			context.EmployeeRoles.AddOrUpdate(er => new {er.EmployeeId,er.RoleId},
				new EmployeeRole{Id = 1,EmployeeId = 1, RoleId = 3},
				new EmployeeRole {Id = 2, EmployeeId = 2, RoleId = 2 },
				new EmployeeRole {Id = 3, EmployeeId = 3, RoleId = 1},
				new EmployeeRole { Id = 4, EmployeeId = 4, RoleId = 3 }
				);
	      
        }
    }
}
