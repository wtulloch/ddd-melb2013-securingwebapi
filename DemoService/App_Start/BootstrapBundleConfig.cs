using System.Web.Optimization;

[assembly: WebActivatorEx.PostApplicationStartMethod(typeof(DemoService.App_Start.BootstrapBundleConfig), "RegisterBundles")]

namespace DemoService.App_Start
{
	public class BootstrapBundleConfig
	{
		public static void RegisterBundles()
		{
			// Add @Styles.Render("~/Content/bootstrap") in the <head/> of your _Layout.cshtml view
			// Add @Scripts.Render("~/bundles/bootstrap") after jQuery in your _Layout.cshtml view
			// When <compilation debug="true" />, MVC4 will render the full readable version. When set to <compilation debug="false" />, the minified version will be rendered automatically

		}
	}
}
