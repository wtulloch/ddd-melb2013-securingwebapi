﻿using System.Web.Http;
using DemoService.Handlers;
using TimeGhost.Security.Core;
using TimeGhost.Security.Tools;
using DemoService.Helpers;

namespace DemoService
{
	public static class WebApiConfig
	{
		public static void Register(HttpConfiguration config)
		{
			//since we only want to use basic authentication when requesting a token we create an
			//expicit route and assign the BasicAuthenticationHandler to it.
			config.Routes.MapHttpRoute(
				name: "TokenApi", 
				routeTemplate:"api/token",
				defaults: new { controller = "Token" },
				constraints: null,
				handler: new BasicAuthenticationHandler(config, GlobalConfiguration.Configuration.DependencyResolver.GetService<IAuthenticationRepository>())
				);

			config.Routes.MapHttpRoute(
				name: "ActionApi",
				routeTemplate: "api/{controller}/{action}/{id}",
				defaults: new { id = RouteParameter.Optional }
			);
			config.Routes.MapHttpRoute(
				name: "DefaultApi",
				routeTemplate: "api/{controller}/{id}",
				defaults: new { id = RouteParameter.Optional}
			);


			config.MessageHandlers.Add(new EnsureHttpsHandler());
			config.MessageHandlers.Add(new TokenValidationHandler(GlobalConfiguration.Configuration.DependencyResolver.GetService<ITokenManager>()));
			
			config.EnableSystemDiagnosticsTracing();
		}
	}
}
