﻿using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using DemoService.Controllers;
using DemoService.Models;
using TimeGhost.Security.Core;
using TimeGhost.Security.Tools;

namespace DemoService
{
	public class AutofacConfig
	{
		 public static void Configure()
		 {
			 var builder = new ContainerBuilder();
			 builder.RegisterApiControllers(Assembly.GetAssembly(typeof (PersonnelController)));
			 builder.RegisterType<EmployeeRepository>().As<IRepository<EmployeeDto>>().SingleInstance();
			 //builder.RegisterInstance(new AuthenticationRepository()) .As<IAuthenticationRepository>();
			 builder.RegisterType<AuthenticationRepository>().As<IAuthenticationRepository>().InstancePerLifetimeScope();
			 builder.RegisterType<TokenManager>().As<ITokenManager>().InstancePerLifetimeScope();
			 var container = builder.Build();

			 DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
			
			 var resolver = new AutofacWebApiDependencyResolver(container);
			 GlobalConfiguration.Configuration.DependencyResolver = resolver;

		 }
	}
}