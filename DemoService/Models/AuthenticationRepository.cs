﻿using System;
using System.Linq;
using TimeGhost.Security.Tools;

namespace DemoService.Models
{
	class AuthenticationRepository : IAuthenticationRepository
	{
		readonly EmployeeContext _dbContext = new EmployeeContext();

		public bool IsAuthenticatedUser(string username, string password)
		{
			return _dbContext.Employees.Any(e => e.Username == username && e.Password == password);
		}

		public User GetUserByUsername(string username)
		{
			var dbUser = _dbContext.Employees.FirstOrDefault(e => e.Username == username);
			if (dbUser == null)
			{
				throw new ArgumentException("username is not valid");
			}

			return ConvertDbUserToUser(dbUser);
		}

		private User ConvertDbUserToUser(Employee dbUser)
		{
			var user = new User
				{
					FirstName = dbUser.FirstName,
					LastName = dbUser.LastName,
					Email = dbUser.Email
				};

			foreach (var employeeRole in dbUser.EmployeeRoles)
			{
				user.Roles.Add(employeeRole.Role.Name);
			}

			return user;
		}
	}
}