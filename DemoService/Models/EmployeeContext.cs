﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DemoService.Models
{
	public class EmployeeContext : DbContext
	{
		public EmployeeContext()
			: base("name=DefaultConnection")
		{
			
		}

		public DbSet<Employee> Employees { get; set; }
		public DbSet<Role> Roles { get; set; }
		public DbSet<EmployeeRole> EmployeeRoles { get; set; }

		}
}