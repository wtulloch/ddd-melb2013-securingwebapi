using System.Data.Entity.ModelConfiguration;

namespace DemoService.Models.Mapping
{
    public class EmployeeRoleMap : EntityTypeConfiguration<EmployeeRole>
    {
        public EmployeeRoleMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("EmployeeRoles");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.EmployeeId).HasColumnName("EmployeeId");
            this.Property(t => t.RoleId).HasColumnName("RoleId");

            // Relationships
            this.HasRequired(t => t.Employee)
                .WithMany(t => t.EmployeeRoles)
                .HasForeignKey(d => d.EmployeeId);
            this.HasRequired(t => t.Role)
                .WithMany(t => t.EmployeeRoles)
                .HasForeignKey(d => d.RoleId);

        }
    }
}
