﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DemoService.Models
{
	public class EmployeeRepository : IRepository<EmployeeDto>
	{
		private List<EmployeeDto> _employees;
		private EmployeeContext _dbContext;
		
		public EmployeeRepository()
		{
			PopulateEmployees();
			_dbContext  = new EmployeeContext();
		}

		

		public List<EmployeeDto> GetAll()
		{
			var employees = new List<EmployeeDto>();
			try
			{
				var employeeQuery =  _dbContext.Employees
					.Include("EmployeeRoles")
					.ToList();
				                      
				foreach (var employee in employeeQuery)
				{
					var employDto = ConvertToDto(employee);

					employees.Add(employDto);
				}
			}
			catch (Exception ex)
			{

				var er = "stop";
			}
			return employees;
		}

		

		public EmployeeDto Get(int id)
		{
			var employee = _dbContext.Employees.FirstOrDefault(e => e.Id == id);
			var dto = ConvertToDto(employee);

			return dto;
		}

		public EmployeeDto Add(EmployeeDto item)
		{
			var lastId = _employees.Max(e => e.Id);
			item.Id = lastId + 1;
			_employees.Add(item);
			return item;
		}

		public EmployeeDto Update(EmployeeDto item)
		{
			var index = _employees.FindIndex(e => e.Id == item.Id);
			_employees[index] = item;
			
			return item;
		}

		public void Delete(int id)
		{
			var employeeToDelete = _dbContext.Employees.FirstOrDefault(e => e.Id == id);
			if (employeeToDelete != null)
			{
				_dbContext.Employees.Remove(employeeToDelete);
				_dbContext.SaveChanges();
			}
		}

		private void PopulateEmployees()
		{
			_employees = new List<EmployeeDto>
				{
					new EmployeeDto{Id = 1, FirstName = "Maureen", LastName = "Tucker", Email = "Maureen.Tucker@TimeGhost.com.au"},
					new EmployeeDto{Id = 2, FirstName = "Lou", LastName = "Reed", Email = "Lou.Reed@TimeGhost.com.au"},
					new EmployeeDto{Id = 3, FirstName = "John", LastName = "Cale", Email = "John.Cale@TimeGhost.com.au"},
				};
		}
		private static EmployeeDto ConvertToDto(Employee employee)
		{
			if (employee == null)
				return null;

			var employDto = new EmployeeDto
			{
				Id = employee.Id,
				FirstName = employee.FirstName,
				LastName = employee.LastName,
				Username = employee.Username,
				Email = employee.Email
			};
			foreach (var employeeRole in employee.EmployeeRoles)
			{
				employDto.Roles.Add(employeeRole.Role.Name);
			}
			return employDto;
		}
	}
}