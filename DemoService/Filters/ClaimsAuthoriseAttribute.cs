﻿using System.Collections.ObjectModel;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace DemoService.Filters
{
	public class ClaimsAuthoriseAttribute : AuthorizeAttribute
	{
		private readonly string _resource;
		private readonly string _action;

		public ClaimsAuthoriseAttribute()
		{
			
		}
		public ClaimsAuthoriseAttribute(string resource, string action)
		{
			_resource = resource;
			_action = action;
		}

		protected override bool IsAuthorized(HttpActionContext actionContext)
		{
			return CheckAccess(actionContext);
		}

		protected virtual bool CheckAccess(HttpActionContext actionContext)
		{

			var resource = _resource ?? actionContext.ControllerContext.ControllerDescriptor.ControllerName;
			var action = _action ?? actionContext.ActionDescriptor.ActionName;

			return ClaimsAuthorisationWrapper.CheckAccess(
				resource,
				action);
		}
	}
}