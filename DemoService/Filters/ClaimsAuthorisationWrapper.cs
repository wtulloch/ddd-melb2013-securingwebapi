﻿using System.IdentityModel.Services;
using System.Security.Claims;

namespace DemoService.Filters
{
	internal static class ClaimsAuthorisationWrapper
	{
		public const string ActionType = "urn:timeghost.claimtypes:action";
		public const string ResourceType = "urn:timeghost.claimtypes:resource";
		public static ClaimsAuthorizationManager AuthorizationManager
		{
			get { return FederatedAuthentication.FederationConfiguration.IdentityConfiguration.ClaimsAuthorizationManager; }
		}

		public static bool CheckAccess(string resource, string action)
		{
			var context = CreateAuthorisationContext(ClaimsPrincipal.Current, resource, action);
			return CheckAccess(context);
		}

		public static bool CheckAccess(AuthorizationContext context)
		{
			return AuthorizationManager.CheckAccess(context);
		}

		public static AuthorizationContext CreateAuthorisationContext(ClaimsPrincipal principal, string resource, string action)
		{
			
			return new AuthorizationContext(principal,resource, action);
		}

	}
}