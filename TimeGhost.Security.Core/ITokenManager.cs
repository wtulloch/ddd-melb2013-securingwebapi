﻿using System.Security.Claims;

namespace TimeGhost.Security.Core
{
	public interface ITokenManager
	{
		TokenResponse CreateJwtToken(ClaimsIdentity claimsIdentity,TokenConfiguration tokenConfig);
		ClaimsPrincipal GetPrincipalFromJwtToken(string token, TokenConfiguration tokenConfig);
	}
}