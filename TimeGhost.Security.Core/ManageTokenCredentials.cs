﻿using System.IdentityModel.Tokens;
using System.ServiceModel.Security.Tokens;
using System.Text;

namespace TimeGhost.Security.Core
{
	public class ManageTokenCredentials
	{
		static readonly Encoding Encoding = Encoding.GetEncoding("iso-8859-1");
		static readonly byte[] TestSymmetricKey = Encoding.GetBytes("This is a test value for creating a symmetric key");
		private const string SignatureAlgorithm = "http://www.w3.org/2001/04/xmldsig-more#hmac-sha256";
		private const string DigestAlgorithm = "http://www.w3.org/2001/04/xmlenc#sha256";

		public static SigningCredentials GetSymmetricKeySigningCredentials()
		{
			var symmetricSecurityKey = new InMemorySymmetricSecurityKey(TestSymmetricKey);
			var signingCredentials = new SigningCredentials(symmetricSecurityKey, SignatureAlgorithm, DigestAlgorithm);

			return signingCredentials;
		}

		public static X509SigningCredentials GetX509SigningCredentials()
		{
			return null;
		}

		public static BinarySecretSecurityToken GetBinarySecretSecurityToken()
		{
			return new BinarySecretSecurityToken(TestSymmetricKey);
		}


	}
}