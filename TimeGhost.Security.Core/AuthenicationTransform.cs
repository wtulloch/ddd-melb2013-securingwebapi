﻿using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Security.Claims;
using TimeGhost.Security.Tools;

namespace TimeGhost.Security.Core
{
	public class AuthenticationTransform : ClaimsAuthenticationManager
	{
		public override ClaimsPrincipal Authenticate(string resourceName, ClaimsPrincipal incomingPrincipal)
		{
			if (incomingPrincipal == null)
			{
				throw new SecurityException("no Principal");
			}

			var roles = incomingPrincipal.Claims.Where(c => c.Type == ClaimTypes.Role)
			                             .Select(c => c.Value).ToList();

			var newClaims = new List<Claim>();
			newClaims.AddRange(incomingPrincipal.Claims);
			newClaims.AddRange(GetNewClaims(roles));

			var claimsIdentity = new ClaimsIdentity(newClaims, "custom");

			return new ClaimsPrincipal(claimsIdentity);


		}

		private List<Claim> GetNewClaims(List<string> roles)
		{
			var claims = new List<Claim>();

			foreach (var role in roles)
			{
				switch (role)
				{
					case Roles.User:
						claims.TryAdd(new Claim(CustomClaimTypes.InformationResource, ClaimValues.Read));
						break;
					case Roles.Developer:
						claims.TryAdd(new Claim(CustomClaimTypes.InformationResource, ClaimValues.Read));
						claims.TryAdd(new Claim(CustomClaimTypes.InformationResource, ClaimValues.Write));
						claims.TryAdd(new Claim(CustomClaimTypes.InformationResource, ClaimValues.Update));
						break;
					case Roles.Manager:
						claims.TryAdd(new Claim(CustomClaimTypes.InformationResource, ClaimValues.Read));
						claims.TryAdd(new Claim(CustomClaimTypes.InformationResource, ClaimValues.Write));
						claims.TryAdd(new Claim(CustomClaimTypes.InformationResource, ClaimValues.Update));
						claims.TryAdd(new Claim(CustomClaimTypes.InformationResource, ClaimValues.Delete));
						break;
				}
			}

			return claims;
		}
	}

	internal static class ExtensionMethods
	{
		public static void TryAdd(this IList<Claim> collection, Claim claim)
		{
			if (!collection.Any(c => c.Type == claim.Type && c.Value == claim.Value))
			{
				collection.Add(claim);
			}
		}
	}
}