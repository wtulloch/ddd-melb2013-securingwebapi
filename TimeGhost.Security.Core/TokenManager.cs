﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Protocols.WSTrust;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Security;
using System.Security.Claims;
using TimeGhost.Security.Tools;

namespace TimeGhost.Security.Core
{
	public class TokenManager : ITokenManager
	{
		private readonly IAuthenticationRepository _repository;

		public TokenManager(IAuthenticationRepository repository)
		{
			_repository = repository;
		}

		public TokenResponse CreateJwtToken(ClaimsIdentity claimsIdentity,TokenConfiguration tokenConfig)
		{
			var user = _repository.GetUserByUsername(claimsIdentity.Name);
			if (user == null)
			{
				throw new SecurityException("Invalid user");
			}

			var now = DateTime.UtcNow;
			var claims = AddUserClaims(user, claimsIdentity);

			
			var token = CreateToken(tokenConfig, claims);

			var tokenResponse = new TokenResponse
				{
					AccessToken = WriteToken(token), 
					TokenType = "Bearer", 
					ExpiresIn = tokenConfig.ExpiresIn *60
				};

			return tokenResponse;
		}




		public ClaimsPrincipal GetPrincipalFromJwtToken(string token, TokenConfiguration tokenConfig)
		{
			var tokenValidationParams = new TokenValidationParameters
			{
				AllowedAudience = tokenConfig.AppliesTo,
				ValidIssuer = tokenConfig.Issuer,
				SigningToken = tokenConfig.SigningSecurityToken,
				ValidateIssuer = true
			};

			return new JwtSecurityTokenHandler().ValidateToken(token, tokenValidationParams);
		}

		private IEnumerable<Claim> AddUserClaims(User user, ClaimsIdentity claimsIdentity)
		{
			var claims = new List<Claim>
				{
					new Claim(ClaimTypes.NameIdentifier, claimsIdentity.Name),
					new Claim(ClaimTypes.Name, claimsIdentity.Name),
					new Claim(ClaimTypes.GivenName, user.FirstName),
					new Claim(ClaimTypes.Surname, user.LastName),
				    new Claim(ClaimTypes.Email, user.Email),
				};
			claims.AddRange(user.Roles.Select(r => new Claim(ClaimTypes.Role,r)));

			return claims;
		}

		private static JwtSecurityToken CreateToken(TokenConfiguration tokenConfig, IEnumerable<Claim> claims)
		{
			var now = DateTime.UtcNow;
			var token = new JwtSecurityToken(
				issuer: tokenConfig.Issuer,
				audience: tokenConfig.AppliesTo,
				claims: claims,
				lifetime: new Lifetime(now, now.AddMinutes(tokenConfig.ExpiresIn)),
				signingCredentials: tokenConfig.SigningCredentials
				);
			return token;
		}

		private static string WriteToken(JwtSecurityToken token)
		{
			return new JwtSecurityTokenHandler().WriteToken(token);
		}
	}
}