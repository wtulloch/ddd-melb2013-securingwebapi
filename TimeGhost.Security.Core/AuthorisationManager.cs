﻿using System.Linq;
using System.Security.Claims;
using TimeGhost.Security.Tools;

namespace TimeGhost.Security.Core
{
	public class AuthorisationManager : ClaimsAuthorizationManager
	{
		public override bool CheckAccess(AuthorizationContext context)
		{
			var resource = context.Resource[0].Value;
			var action = context.Action[0].Value;
			var principal = context.Principal;

			if (resource == "Information")
			{
				var allowAccess = false;
				if (!principal.HasClaim(c => c.Type == CustomClaimTypes.InformationResource))
				{
					return false;
				}
				var allowActions = principal.Claims.Where(c => c.Type == CustomClaimTypes.InformationResource)
				                            .Select(c => c.Value)
				                            .ToList();
				switch (action.ToLower())
				{
					case "get":
						allowAccess = allowActions.Contains(ClaimValues.Read);
						break;
					case "post":
						allowAccess = allowActions.Contains(ClaimValues.Write);
						break;
					case "put":
						allowAccess = allowActions.Contains(ClaimValues.Update);
						break;
					case "delete":
						allowAccess = allowActions.Contains(ClaimValues.Delete);
						break;
					case "secrets":
						allowAccess = principal.HasClaim(c => c.Type == ClaimTypes.Name && c.Value == "edward.snowden");
						break;
				}

				return allowAccess;
			}

			return true;
		}
	}
}