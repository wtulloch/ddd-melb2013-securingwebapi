﻿using System.IdentityModel.Tokens;

namespace TimeGhost.Security.Core
{
	public class TokenConfiguration
	{
		public string Issuer { get; set; }
		public string AppliesTo { get; set; }
		public int ExpiresIn { get; set; }
		public SigningCredentials SigningCredentials { get; set; }
		public SecurityToken SigningSecurityToken { get; set; }
	}
}